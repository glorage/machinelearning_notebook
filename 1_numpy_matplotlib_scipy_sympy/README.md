# Numpy, Matplotlib, Scipy等常用库

## 内容
* [numpy教程](1-numpy_tutorial.ipynb)
* matplotlib
    - [matplotlib简易教程](2-matplotlib_tutorial.ipynb)
    - [matplotlib系统学习](matplotlib_full.ipynb)
* [ipython](3-ipython_notebook.ipynb)
* [scipy](4-scipy_tutorial.ipynb)
* [sympy](5-sympy_tutorial.ipynb)

## 扩展内容
* [git introduction](utils_git.ipynb)
* [git workflow](utils_git_advanced.ipynb)
* [shell](utils_shell.ipynbs)

## References
* [手把手教你用Python做数据可视化](https://mp.weixin.qq.com/s/3Gwdjw8trwTR5uyr4G7EOg)
* [Essential Cheat Sheets for deep learning and machine learning researchers](https://github.com/kailashahirwar/cheatsheets-ai)
