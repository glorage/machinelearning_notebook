# Python进阶

## 课程介绍
Python 是一个高层次的结合了解释性、编译性、互动性和面向对象的脚本语言。本节课讲解更深入的Python知识

## 课程目录

* [语句](more1.md) 
* [迭代器](more2.md) 
* [文件与os模块](more3.md) 
* [函数](more4.md) 
* [面向对象编程](more5.md)



## 参考资料

* [and or & | 的区别](https://blog.csdn.net/weixin_40041218/article/details/80868521)
* [range xrange 区别](http://www.nowamagic.net/academy/detail/1302446)
* [with open() as f](https://www.cnblogs.com/ymjyqsx/p/6554817.html)
* [含参数继承](https://www.cnblogs.com/kongk/p/8644745.html)