
# 简明Python教程 （90分钟学会Python）

Python 是一门上手简单、功能强大、通用型的**脚本**编程语言。

* Python 类库丰富，这使得 Python 几乎无所不能，网站开发、软件开发、大数据分析、网络爬虫、机器学习等都不在话下；
* Python最主要的优点是使用人类的思考方式来编写程序，大多数情况下使用封装好的库能够快速完成给定的任务，虽然执行的效率不一定很高，但是极大的缩短了程序设计、编写、调试的时间，因此非常适合快速学习、尝试、试错。

关于Python的安装可以参考[《安装Python环境》](../references_tips/InstallPython.md)，或者自行去网络上查找相关的资料。

为方便大家更好地学习并掌握Python，本教程配套[《90分钟学会Python-在线视频》](https://www.bilibili.com/video/BV1oZ4y1N7ei?p=9)，请观看学习。

学习Python最好的方式是编程练习与小项目，请大家认真完成[Python编程练习题](https://gitee.com/pi-lab/machinelearning_homework/tree/master/homework_01_python)。

![learn python](images/learn_python.jpg)

## 内容
0. [Install Python](../references_tips/InstallPython.md)
1. [IPython & Jupyter Notebook](0-ipython_notebook.ipynb)
2. [Basics](1_Basics.ipynb)
    - Why Python, Zen of Python
    - Variables, Operators, Built-in functions
3. [Print statement](2_Print_Statement.ipynb)
    - Tips of print
4. [Data structure - 1](3_Data_Structure_1.ipynb)
    - Lists, Tuples, Sets
5. [Data structure - 2](4_Data_Structure_2.ipynb)
    - Strings, Dictionaries
6. [Control flow](5_Control_Flow.ipynb)
    - if, else, elif, for, while, break, continue
7. [Functions](6_Function.ipynb)
    - Function define, return, arguments
    - Gloabl and local variables
    - Lambda functions
8. [Class](7_Class.ipynb)
    - Class define
    - Inheritance

## 其他教学材料

* [Python基础](python/hellopython)
    - 编程语言特性
    - 程序结构
    - 基础技术框架
    - 编程风格
    - 对象特性
    - 核心数据类型，字符串，列表，元祖，字典
    - 表达式与运算符
* [Python进阶](python/more)
    - if,while,for
    - 迭代器，生成器
    - 文件对象
    - os模块，os模块常用接口
    - 函数
    - 类，继承，重载 
* [Python模块化编程](python/module)
    - 模块基础，模块机制
    - 异常处理
    - 常用科学计算模块



除了学习内容之外，本教程还包括一些[例子程序](demo_codes)





## 参考资料

### 视频教程
* [《90分钟学会Python》](https://www.bilibili.com/video/BV1oZ4y1N7ei?p=9) （推荐）
* [《零基础入门学习Python》教学视频](https://www.bilibili.com/video/BV1c4411e77t)

### 教程
* [安装Python环境](../references_tips/InstallPython.md)
* [IPython Notebooks to learn Python](https://github.com/rajathkmp/Python-Lectures)
* [廖雪峰的Python教程](https://www.liaoxuefeng.com/wiki/1016959663602400)
* [跟海龟学Python](https://gitee.com/pi-lab/python_turtle)
* [智能系统实验室入门教程-Python](https://gitee.com/pi-lab/SummerCamp/tree/master/python)
* [Python Tips](../references_tips/python)
* [Get Started with Python](Python.pdf)
* [Python - 100天从新手到大师](https://github.com/jackfrued/Python-100-Days)
* [Python Tutorial essential Training](https://github.com/hussien89aa/PythonTutorial)
* [Beginners book on Python: byte-of-python](https://github.com/swaroopch/byte-of-python)
* [A Byte of Python](refences/byte-of-python.pdf)
* [Useful functions, tutorials, and other Python-related things](https://github.com/rasbt/python_reference)

### 练习题

* [Python编程练习题](https://gitee.com/pi-lab/machinelearning_homework/tree/master/homework_01_python)

* [Python 练习册，每天一个小程序 show-me-the-code](https://github.com/Yixiaohan/show-me-the-code)