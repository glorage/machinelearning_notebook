# 神经网络

人工神经网络（artificial neural network，ANN），简称神经网络（neural network，NN），是一种模仿生物神经网络的结构和功能的数学模型或计算模型，神经网络由大量的人工神经元联结进行计算。大多数情况下人工神经网络能在外界信息的基础上改变内部结构，是一种自适应系统。现代神经网络是一种非线性统计性数据建模工具，常用来对输入和输出间复杂的关系进行建模，或用来探索数据的模式。

![mlp_theory](images/mlp_theory.gif)

## 内容

* [感知机](1-Perceptron.ipynb)

* [多层神经网络和反向传播](2-mlp_bp.ipynb)

* [Softmax和交叉熵](3-softmax_ce.ipynb)

    

## References

* https://iamtrask.github.io/2015/07/12/basic-python-network/
* http://www.wildml.com/2015/09/implementing-a-neural-network-from-scratch/

